defmodule Nodes.MixProject do
  use Mix.Project

  def project do
    [
      app: :nodes,
      version: "0.1.0",
      elixir: "~> 1.8",
      start_permanent: Mix.env() == :prod,
      deps: deps(),

      # Docs
      name: "Nodes",
      source_url: "https://gitlab.com/aicacia/ex-nodes",
      homepage_url: "https://gitlab.com/aicacia/ex-nodes",
      docs: [
        main: "Nodes",
        extras: ["README.md"]
      ]
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:uuid, "~> 1.1"},
      {:ex_doc, "~> 0.19", only: :dev, runtime: false}
    ]
  end
end
