defmodule Actions do
  defmodule Count do
    use Nodes.Actions.AsyncAction
  end
end

defmodule Root do
  use Nodes.Node, children: [Counter]
end

defmodule Counter do
  use Nodes.Node, initial_state: %{count: 0}

  def handle(%{} = action, state) do
    cond do
      Actions.Count.is(action) ->
        {root_pid, count} = action.payload

        if count > 0 do
          Root.dispatch(
            root_pid,
            Actions.Count.Fulfilled.create(action, count)
          )
        else
          Root.dispatch(root_pid, Actions.Count.Rejected.create(action))
        end

        state

      Actions.Count.Fulfilled.is(action) ->
        %{count: state.count + action.payload}

      Actions.Count.Rejected.is(action) ->
        %{count: 0}

      true ->
        state
    end
  end
end

defmodule NodesTest do
  use ExUnit.Case
  doctest Nodes

  test "count async fulfilled" do
    {:ok, root_pid} = Root.start_link(:spawn)

    Root.dispatch(root_pid, Actions.Count.create({root_pid, 1}))
    IO.inspect(Root.state(root_pid))
    assert Root.state(root_pid).children[Counter].state.count == 1
  end

  test "count async rejected" do
    {:ok, root_pid} = Root.start_link(:spawn)

    Root.dispatch(root_pid, Actions.Count.create({root_pid, 1}))
    IO.inspect(Root.state(root_pid))
    assert Root.state(root_pid).children[Counter].state.count == 1

    Root.dispatch(root_pid, Actions.Count.create({root_pid, -1}))
    IO.inspect(Root.state(root_pid))
    assert Root.state(root_pid).children[Counter].state.count == 0
  end

  test "streams" do
    {:ok, root_pid} = Root.start_link(:spawn)
    {:ok, stream} = Root.stream(root_pid)

    action = Actions.Count.create({root_pid, 1})
    Root.dispatch(root_pid, action)

    fulfilled =
      stream
      |> Stream.filter(fn a ->
        Actions.Count.Fulfilled.is_ref(action, a)
      end)
      |> Enum.at(0)

    assert Actions.Count.Fulfilled.is(fulfilled)
  end
end
