defmodule Nodes.Node do
  defmacro __using__(opts \\ []) when is_list(opts) do
    quote do
      use Supervisor
      import Stream, [:resource]

      @callback handle(Map.t(), term) :: term
      def handle(%{} = action, state) do
        state
      end

      @callback on_dispatch(Process.t(), Map.t()) :: term
      def on_dispatch(pid, %{} = action) do
        nil
      end

      defoverridable handle: 2, on_dispatch: 2
      defstruct state: nil, children: %{}

      def options() do
        unquote(opts)
      end

      def initial_state() do
        options()[:initial_state]
      end

      defmodule State do
        use GenServer

        def parent() do
          __MODULE__ |> Module.split() |> Enum.drop(-1) |> Module.concat()
        end

        def start_link(_opts \\ []) do
          GenServer.start_link(__MODULE__, parent().initial_state())
        end

        def dispatch(pid, %{} = action) do
          GenServer.cast(pid, {:dispatch, action})
        end

        def state(pid) do
          :sys.get_state(pid)
        end

        @impl true
        def init(state) do
          {:ok, state}
        end

        @impl true
        def handle_cast({:dispatch, action}, state) do
          new_state = parent().handle(action, state)
          {:noreply, new_state}
        end
      end

      defmodule Stream do
        use GenServer

        defstruct [:caller_pid]

        def start_link(caller_pid) do
          GenServer.start_link(__MODULE__, %__MODULE__{caller_pid: caller_pid})
        end

        def dispatch(pid, %{} = action) do
          GenServer.cast(pid, {:dispatch, action})
        end

        @impl true
        def init(state) do
          {:ok, state}
        end

        @impl true
        def handle_cast({:dispatch, action}, %__MODULE__{caller_pid: caller_pid} = state) do
          send(caller_pid, {:dispatch, action})
          {:noreply, state}
        end
      end

      defmodule Streams do
        use DynamicSupervisor

        def start_link(_opts \\ []) do
          DynamicSupervisor.start_link(__MODULE__, [], name: __MODULE__)
        end

        def dispatch(pid, %{} = action) do
          Supervisor.which_children(pid)
          |> Enum.each(fn {_id, child, _types, _modules} ->
            Stream.dispatch(child, action)
          end)
        end

        @impl true
        def init(_opts) do
          DynamicSupervisor.init(strategy: :one_for_one)
        end
      end

      def stream() do
        stream(__MODULE__)
      end

      def stream(pid) do
        case DynamicSupervisor.start_child(get_streams_pid(pid), {__MODULE__.Stream, self()}) do
          {:ok, stream_pid} ->
            {:ok,
             resource(
               fn ->
                 stream_pid
               end,
               fn stream_pid ->
                 receive do
                   {:dispatch, action} ->
                     {[action], stream_pid}
                 after
                   60_000 ->
                     {:halt, stream_pid}
                 end
               end,
               fn stream_pid ->
                 DynamicSupervisor.terminate_child(pid, stream_pid)
               end
             )}

          result ->
            result
        end
      end

      defp get_streams_pid() do
        get_streams_pid(__MODULE__)
      end

      defp get_streams_pid(pid) do
        children(pid)
        |> Enum.find_value(fn {id, child, _types, _modules} ->
          if id == Streams do
            child
          end
        end)
      end

      def start_link() do
        Supervisor.start_link(__MODULE__, [], name: __MODULE__)
      end

      def start_link(:spawn) do
        Supervisor.start_link(
          Enum.map(options()[:children] || [], fn child ->
            {child, :spawn}
          end) ++ [State, Streams],
          strategy: :one_for_one
        )
      end

      @impl true
      def init(_opts) do
        Supervisor.init(
          Enum.map(options()[:children] || [], fn child ->
            {child, :spawn}
          end) ++ [State, Streams],
          strategy: :one_for_one
        )
      end

      def children() do
        children(__MODULE__)
      end

      def children(pid) do
        Supervisor.which_children(pid)
      end

      def state() do
        state(__MODULE__)
      end

      def state(pid) do
        children(pid)
        |> Enum.reduce(%__MODULE__{}, fn {id, child, _types, _modules}, acc ->
          if id == Streams do
            acc
          else
            state = apply(id, :state, [child])

            if id == State do
              Map.put(acc, :state, state)
            else
              Map.update(acc, :children, [], fn children ->
                Map.put(children, id, state)
              end)
            end
          end
        end)
      end

      def dispatch(%{} = action) do
        dispatch(__MODULE__, action)
      end

      def dispatch(pid, %{} = action) do
        on_dispatch(pid, action)

        children(pid)
        |> Enum.each(fn {id, child, _types, _modules} ->
          apply(id, :dispatch, [child, action])
        end)
      end
    end
  end
end
