defmodule Nodes.Actions.RefAction do
  defmacro __using__(ref_action_type) do
    quote do
      def ref_type do
        unquote(ref_action_type)
      end

      def type do
        String.downcase(to_string(__MODULE__))
      end

      defstruct [:id, :ref_id, :type, :ref_type, :payload, :meta]

      def is(action),
        do:
          (Map.has_key?(action, "ref_id") or Map.has_key?(action, :ref_id)) and
            type() == Map.get(action, "type", Map.get(action, :type))

      def is_ref(action, ref_action),
        do:
          Map.get(action, "id", Map.get(action, :id)) ==
            Map.get(ref_action, "ref_id", Map.get(ref_action, :ref_id)) and
            ref_type() == Map.get(ref_action, "ref_type", Map.get(ref_action, :ref_type)) and
            type() == Map.get(ref_action, "type", Map.get(ref_action, :type))

      def create(action),
        do: create(action, nil, nil)

      def create(action, payload),
        do: create(action, payload, nil)

      def create(action, payload, meta),
        do: %__MODULE__{
          id: UUID.uuid1(),
          ref_id: Map.get(action, "id", Map.get(action, :id)),
          type: type(),
          ref_type: ref_type(),
          payload: payload,
          meta: meta
        }
    end
  end
end
