defmodule Nodes.Actions.Action do
  def type_string(value) do
    String.downcase(to_string(value))
  end

  defmacro __using__(action_type) when is_bitstring(action_type) do
    quote do
      def type do
        unquote(action_type)
      end

      defstruct [:id, :type, :payload, :meta]

      def is(action), do: type() == Map.get(action, "type", Map.get(action, :type))

      def create(payload), do: create(payload, nil)

      def create(payload, meta),
        do: %__MODULE__{id: UUID.uuid1(), type: type(), payload: payload, meta: meta}
    end
  end

  defmacro __using__(_opts) do
    quote do
      def type do
        Nodes.Actions.Action.type_string(__MODULE__)
      end

      defstruct [:id, :type, :payload, :meta]

      def is(action), do: type() == Map.get(action, "type", Map.get(action, :type))

      def create(payload), do: create(payload, nil)

      def create(payload, meta),
        do: %__MODULE__{id: UUID.uuid1(), type: type(), payload: payload, meta: meta}
    end
  end
end
