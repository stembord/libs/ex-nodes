defmodule Nodes.Actions.AsyncAction do
  defmacro __using__(_opts) do
    quote do
      use Nodes.Actions.Action

      defmodule Fulfilled do
        use Nodes.Actions.RefAction,
            Nodes.Actions.Action.type_string(
              __MODULE__
              |> Module.split()
              |> Enum.drop(-1)
              |> Module.concat()
            )
      end

      defmodule Rejected do
        use Nodes.Actions.RefAction,
            Nodes.Actions.Action.type_string(
              __MODULE__
              |> Module.split()
              |> Enum.drop(-1)
              |> Module.concat()
            )
      end
    end
  end
end
